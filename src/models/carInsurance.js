/**
 * @classdesc carInsurance is a insurance of car.
 *
 * @name carInsurance
 * @class
 */

class carInsurance {

    constructor( products = []) {
        this.products = products;
    }
    
    /**
     * @function getProducts
     * @description Get all de products added
     */

    getProducts() {
        return this.products;
    }

    /**
     * @function updatePrice
     * @description Update the price of each product as the days go by.
     */

    updatePrice() {
        for (var i = 0; i < this.products.length; i++) {
            this.products[i]
            this.products[i].update();
        }
        return this.products;
    }
}

module.exports = carInsurance;
