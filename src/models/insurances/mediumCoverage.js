const Product = require('../product');
/**
 * @classdesc MediumCoverage is a secondary product of the product.
 *
 * @name MediumCoverage
 * @class
 * @extends Product
 */

class MediumCoverage extends Product {

    static nameCoverage = 'Medium Coverage';

    /**
     * Create a Medium Coverage.
     * @param {string} name - The name of secondary product.
     * @param {number} sellIn - The limit of day to sell.
     * @param {number} price - The secondary product price.
     */
    
    constructor(name, sellIn, price) {
        super(name, sellIn, price)
    }
    
}

module.exports = MediumCoverage;