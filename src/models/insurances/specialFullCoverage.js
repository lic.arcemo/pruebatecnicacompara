const Product = require('../product');

/**
 * @classdesc specialFullCoverage coverage is a secondary product of the product.
 *
 * @name specialFullCoverage
 * @class
 * @extends Product
 */

class specialFullCoverage extends Product {

    static nameCoverage = 'Special Full Coverage'

    /**
     * Create a Special Full Coverage.
     * @param {string} name - The name of secondary product.
     * @param {number} sellIn - The limit of day to sell.
     * @param {number} price - The secondary product price.
     */

    constructor(name, sellIn, price){
        super(name, sellIn,price);
    }

    /**
     * (Override) price increases by 2 when there are 10 days or less and by 3 when there are 5 days or less but.
     * (Override) price drops to 0 when no more days left (and the product is not valid anymore).
     * @override
    */

    setPrice() {
        if (this.sellIn <= 0) {
            
            this.price = 0;

        } else if ( this.sellIn <= 10 && this.sellIn > 5){

            this.price = this.price + 2;

        } else if ( this.sellIn <= 5 ){

            this.price = this.price + 3;

        } else {
            this.price = this.price + 1;
        }
        this.checkPrice();
      }
}

module.exports = specialFullCoverage;