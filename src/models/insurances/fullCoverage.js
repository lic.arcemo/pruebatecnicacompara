const Product = require('../product');

/**
 * @classdesc Full coverage is a secondary product of the product.
 *
 * @name FullCoverage
 * @class
 * @extends Product
 */

class FullCoverage extends Product {

    static nameCoverage = 'Full Coverage'

    /**
     * Create a Full Coverage.
     * @param {string} name - The name of secondary product.
     * @param {number} sellIn - The limit of day to sell.
     * @param {number} price - The secondary product price.
     */

    constructor(name, sellIn, price){
        super(name, sellIn,price)
    }

    /**
     * (Override) Update the price and check the price for see if it is more than 50, update it 
     * @override
     */

    setPrice() {
        if (this.sellIn <= 0) {
            this.price = this.price + (Product.defaultPriceDown * 2);
        } else {
        this.price = this.price + Product.defaultPriceDown ;
        }
        super.checkPrice();
      }
}

module.exports = FullCoverage;