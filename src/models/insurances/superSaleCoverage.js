const Product = require('../product');

/**
 * @classdesc Super Sale is a secondary product of the product.
 *
 * @name SuperSaleCoverage
 * @class
 * @extends Product
 */

class SuperSaleCoverage extends Product {

    static nameCoverage = 'Super Sale';

    /**
     * Create a Super Sale.
     * @param {string} name - The name of secondary product.
     * @param {number} sellIn - The limit of day to sell.
     * @param {number} price - The secondary product price.
    */

    constructor(name, sellIn, price) {
        super(name, sellIn, price)
    }

   /**
     * (Override) SuperSale decrease twice fast than default product
     * @override
    */
    setPrice() {
        if (this.sellIn <= 0) {
            this.price = this.price - ((Product.defaultPriceDown * 2) * 2 );
        } else {
            this.price = this.price - (Product.defaultPriceDown * 2) ;
        };
        this.checkPrice();
    }
}

module.exports = SuperSaleCoverage;