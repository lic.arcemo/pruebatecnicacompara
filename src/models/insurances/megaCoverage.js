const Product = require('../product');

/**
 * @classdesc MegaCoverage is a secondary product of the product.
 *
 * @name MegaCoverage
 * @class
 * @extends Product
 */

class MegaCoverage extends Product {

    static nameCoverage     = 'Mega Coverage';
    static maxPrice         = 80;
    static defaultSellIn    = 0;

    /**
     * Create a Mega Coverage.
     * @param {string} name - The name of secondary product.
     * @param {number} sellIn - The limit of day to sell.
     * @param {number} price - The secondary product price.
     */

    constructor(name) {
        super(name);
        this.price     = MegaCoverage.maxPrice;
        this.sellIn    = MegaCoverage.defaultSellIn;
    }

    /**
     * (Override) Update the price because the price of the mega coverage should always be 80
     * @override
    */
    setPrice() {
        this.price     = MegaCoverage.maxPrice;
    }

    /**
     * (Override) Update the sellin variable to default because is a legendary product and will never be sold
     * @override
    */
    setSellIn() {
        this.sellIn    = MegaCoverage.defaultSellIn;
    }
}

module.exports = MegaCoverage;