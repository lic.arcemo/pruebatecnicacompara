const Product = require('../product');

/**
 * @classdesc LowCoverage is a secondary product of the product.
 *
 * @name LowCoverage
 * @class
 * @extends Product
 */


class LowCoverage extends Product {

    static nameCoverage = 'Low Coverage';

    /**
     * Create a Low Coverage.
     * @param {string} name - The name of secondary product.
     * @param {number} sellIn - The limit of day to sell.
     * @param {number} price - The secondary product price.
     */

    constructor(name, sellIn, price) {
        super(name, sellIn, price)
    }
}

module.exports = LowCoverage;