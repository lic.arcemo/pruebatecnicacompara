/**
 * @classdesc The product class
 *
 * @name Product
 * @class
 */

class Product {

    static defaultPriceDown = 1;
    static defaultDaysToDown = 1;
    static maxPrice = 50;
    /**
     * Create a Product.
     * @param {string} name - The name of product.
     * @param {number} sellIn - The limit of day to sell.
     * @param {number} price - The product price.
     */
    
    constructor(name, sellIn, price) {
      this.name = name;
      this.sellIn = sellIn;
      this.price = price;
      this.checkPrice();
    }

    /**
     * @function update 
     * @description call to update price and sellIn
    */

    update() {
        this.setPrice();
        this.setSellIn();
    }

    /**
     * @function setPrice 
     * @description update price to the default price to go down and if the sell in variable is less than 0, it degrade twice more fast.
     */

    setPrice(){
        if (this.sellIn <= 0) {
            this.price = this.price - (Product.defaultPriceDown * 2);
        } else {
            this.price = this.price - Product.defaultPriceDown ;
        }
        this.checkPrice();
    }

    /**
     * @function setSellIn 
     * @description update day limit of sell
    */

    setSellIn(){
        this.sellIn = this.sellIn - Product.defaultDaysToDown;
    }
    
    /**
     * @function checkPrice 
     * @description check the price when the price is more than max Price, it degrade to max Price and if it is less tna 0, it set to 0. 
    */

    checkPrice() {
        if (this.price >= Product.maxPrice) this.price = 50;
        if (this.price <= 0) this.price = 0;
      }

  }
  
  
  module.exports = Product;