const MediumCoverage = require('../models/insurances/mediumCoverage')
const MegaCoverage   = require('../models/insurances/MegaCoverage')
const FullCoverage   = require('../models/insurances/fullCoverage')
const SpecialFullCoverage = require('../models/insurances/specialFullCoverage')
const SuperSaleCoverage   = require('../models/insurances/superSaleCoverage')
const LowCoverage         = require('../models/insurances/lowCoverage')

/** @module productController */

class productController {

    /** 
     * @function addProduct 
     * @description Add  a new product to the correct category
    */
    addProduct (name, sellIn, price) {
        switch (name) {
            case (MediumCoverage.nameCoverage):
                return new MediumCoverage(name, sellIn, price);
            break;
            case (MegaCoverage.nameCoverage):
                return new MegaCoverage(name);
            break;
            case (FullCoverage.nameCoverage):
                return new FullCoverage(name, sellIn, price);
            break;
            case (SpecialFullCoverage.nameCoverage):
                return new SpecialFullCoverage(name, sellIn, price);
            break;
            case (SuperSaleCoverage.nameCoverage):
                return new SuperSaleCoverage(name, sellIn, price);
            break;
            case (LowCoverage.nameCoverage):
                return new LowCoverage(name, sellIn, price);
            break;
            default:
                break;
        }
    }
} 

module.exports = {
    addProduct: new productController().addProduct
}
