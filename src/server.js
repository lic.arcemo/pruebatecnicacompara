
const addProduct    = require('./controllers/productController').addProduct;
const CarInsurance  = require('./models/carInsurance');

/** @module Application */

/** 
 * @name Application
 * @description Start the application: "npm run after-30-days"
 *  */



/**
 * @function start
 * @description start the application
 */
function start (){
    const productsAtDayZero = [
        addProduct('Medium Coverage', 10, 20),
        addProduct('Full Coverage', 2, 0),
        addProduct('Low Coverage', 5, 7),
        addProduct('Mega Coverage', 0, 80),
        addProduct('Mega Coverage', -1, 80),
        addProduct('Special Full Coverage', 15, 20),
        addProduct('Special Full Coverage', 10, 49),
        addProduct('Special Full Coverage', 5, 49),
        addProduct('Super Sale', 3, 6)
    ];
    
    const carInsurance = new CarInsurance(productsAtDayZero);
    const productPrinter = function (product) {
      console.log(`${product.name}, ${product.sellIn}, ${product.price}`);
    };
    
    for (let i = 1; i <= 30; i += 1) {
      console.log(`Day ${i}`);
      console.log('name, sellIn, price');
      carInsurance.updatePrice().forEach(productPrinter);
      console.log('');
    }
}

start();
