const expect = require('chai').expect;
const MegaCoverage = require('../../../src/models/insurances/megaCoverage');

describe("Mega Coverage", function() {
    
  it("should create a new product with the correct values", function() {
    const product = new MegaCoverage('Mega Coverage',2,0);
    expect(product.name).equal(MegaCoverage.nameCoverage);
    expect(product.sellIn).equal(0);
    expect(product.price).equal(80);
  });

  it("Should has a price of 80 and never has to be sold sellIn 0. ", function() {
    const product = new MegaCoverage('Mega Coverage',10,50);
    expect(product.name).equal(MegaCoverage.nameCoverage);
    expect(product.sellIn).equal(0);
    expect(product.price).equal(80);
  });

  it("Should never change the price and sell after one day ", function() {
    const product = new MegaCoverage('Full Coverage',0,80);
    product.update();
    expect(product.sellIn).equal(0);
    expect(product.price).equal(80);
  });
  
});
