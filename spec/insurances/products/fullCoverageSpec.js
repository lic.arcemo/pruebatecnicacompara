const expect = require('chai').expect;
const FullCoverage = require('../../../src/models/insurances/fullCoverage');

describe("Full Coverage", function() {
    
  it("should create a new product", function() {
    const product = new FullCoverage('Full Coverage',2,0);
    expect(product.name).equal(FullCoverage.nameCoverage);
    expect(product.sellIn).equal(2);
    expect(product.price).equal(0);
  });

  it("should increase price when the older it gets. from 0 to 1", function() {
    const product = new FullCoverage('Full Coverage',2,0);
    product.update();
    expect(product.price).equal(1);
  });


  it("Once the sell by date has passed, price increase twice as fast. from 40 to 42", function() {
    const product = new FullCoverage('Full Coverage',0,40);
    product.update();
    expect(product.sellIn).equal(-1);
    expect(product.price).equal(42);
  });


  it("Should set the price if it is more than 50. From 54 to 50", function() {
    const product = new FullCoverage('Full Coverage',2,54);
    expect(product.price).equal(50);
  });
  
  it("should keep the price at maximum alowed", function () {
    const product = new FullCoverage('Full Coverage',1,50);
    product.update();
    expect(product.sellIn).equal(0);
    expect(product.price).equal(50);
});



});
