const expect = require('chai').expect;
const SpecialFullCoverage = require('../../../src/models/insurances/specialFullCoverage');

describe("Special Full Coverage", function() {
    
  it("should create a new Special Full Coverage product", function() {
    const product = new SpecialFullCoverage('Special Full Coverage',2,10);
    expect(product.name).equal(SpecialFullCoverage.nameCoverage);
    expect(product.sellIn).equal(2);
    expect(product.price).equal(10);
  });

  it("should increase price when the older it gets and sellIn more than 10. from 12 to 13", function() {
    const product = new SpecialFullCoverage('Special Full Coverage',20,12);
    product.update();
    expect(product.sellIn).equal(19);
    expect(product.price).equal(13);
  });

  it("should price increases by 2 when there are 10 days or 6 ", function() {
    const product = new SpecialFullCoverage('Special Full Coverage',7,10);
    product.update();
    expect(product.sellIn).equal(6);
    expect(product.price).equal(12);
  });

  it("should price increases by 3 when there are 5 days or less ", function() {
    const product = new SpecialFullCoverage('Special Full Coverage',5,10);
    product.update();
    expect(product.sellIn).equal(4);
    expect(product.price).equal(13);
  });

  it("should the price drops to 0 when no more days left ", function() {
    const product = new SpecialFullCoverage('Special Full Coverage',0,10);
    product.update();
    expect(product.sellIn).equal(-1);
    expect(product.price).equal(0);
  });


});