const expect = require('chai').expect;
const SuperSaleCoverage = require('../../../src/models/insurances/superSaleCoverage');

describe("Super Sale Coverage", function() {
    
  it("should create a new Super Sale Coverage Product ", function() {
    const product = new SuperSaleCoverage('Super Sale',2,10);
    expect(product.name).equal(SuperSaleCoverage.nameCoverage);
    expect(product.sellIn).equal(2);
    expect(product.price).equal(10);
  });

  it("should degrade price twice as fast as normal Products when 'SellIn' more than 0. Price from 10 to 8", function() {
    const product = new SuperSaleCoverage('Super Sale',10,10);
    product.update();
    expect(product.sellIn).equal(9);
    expect(product.price).equal(8);
  });
 
  it("should degrade price twice as fast as normal Products when the 'SellIn' in less than 0. Price from 10 to 6", function() {
    const product = new SuperSaleCoverage('Super Sale',0,10);
    product.update();
    expect(product.sellIn).equal(-1);
    expect(product.price).equal(6);
  });
 

});