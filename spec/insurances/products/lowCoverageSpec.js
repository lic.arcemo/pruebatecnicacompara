const expect = require('chai').expect;
const LowCoverage = require('../../../src/models/insurances/lowCoverage');

describe("Low Coverage", function() {
    
  it("should create a new product", function() {
    const product = new LowCoverage('Low Coverage',2,10);
    expect(product.name).equal(LowCoverage.nameCoverage);
    expect(product.sellIn).equal(2);
    expect(product.price).equal(10);
  });

  it("should decrease price when the older it gets. from 10 to 9", function() {
    const product = new LowCoverage('Full Coverage',2,10);
    product.update();
    expect(product.price).equal(9);
  });

  it("Once the sell by date has passed, price degrades twice as fast from 2 to 1", function() {
    const product = new LowCoverage('Lower Coverage',-1,40);
    product.update();
    expect(product.sellIn).equal(-2);
    expect(product.price).equal(38);
  });

  it("Should set the price if it is more than 50. From 54 to 50", function() {
    const product = new LowCoverage('Low Coverage',1,54);
    expect(product.price).equal(50);
  });

  it("should keep price at maximum alowed", function () {
    const product = new LowCoverage('Low Coverage',1,54);
    expect(product.sellIn).equal(1);
    expect(product.price).equal(50);
});



});
