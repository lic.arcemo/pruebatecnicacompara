const expect = require('chai').expect;
const MediumCoverage = require('../../../src/models/insurances/mediumCoverage');

describe("Medium Coverage", function() {
    
  it("should create a new Medium Coverage product", function() {
    const product = new MediumCoverage('Medium Coverage',2,10);
    expect(product.name).equal(MediumCoverage.nameCoverage);
    expect(product.sellIn).equal(2);
    expect(product.price).equal(10);
  });

  it("should decrease price when the older it gets. from 10 to 9", function() {
    const product = new MediumCoverage('Medium Coverage',2,10);
    product.update();
    expect(product.price).equal(9);
  });

  it("Once the sell by date has passed, price degrades twice as fast from 2 to 1", function() {
    const product = new MediumCoverage('Lower Coverage',-1,40);
    product.update();
    expect(product.sellIn).equal(-2);
    expect(product.price).equal(38);
  });

  it("Should set the price if it is more than 50. From 60 to 50", function() {
    const product = new MediumCoverage('Low Coverage',1,60);
    expect(product.price).equal(50);
  });

  it("should keep price at maximum alowed when create a new product", function () {
    const product = new MediumCoverage('Low Coverage',1,54);
    expect(product.sellIn).equal(1);
    expect(product.price).equal(50);
});


});